import React, { useState, useEffect } from 'react';
import './Slider.css';

const Slider = () => {
  const [isDarkMode, setDarkMode] = useState(false);

  useEffect(() => {
    const body = document.body;
    if (isDarkMode) {
      body.classList.add('dark');
    } else {
      body.classList.remove('dark');
    }
  }, [isDarkMode]);

  const changeTheme = () => {
    setDarkMode(!isDarkMode);
  };

  const sliderClassName = `slider ${isDarkMode ? 'dark' : ''}`;

  return (
    <div>
      <h1 style={{ color: '#999', fontSize: '19px' }}>Solar System Planets</h1>
      <label className="switch" htmlFor="checkbox" onClick={changeTheme}>
        <input type="checkbox" id="checkbox" />
        <div
          className={`slider round ${sliderClassName}`}
          onClick={(e) => e.stopPropagation()}
        ></div>
      </label>
      <ul className={`planets-list ${isDarkMode ? 'dark' : ''}`}>
        <li style={{ color: isDarkMode ? '#66bb6a' : 'black' }}>Mercury</li>
        <li style={{ color: isDarkMode ? '#66bb6a' : 'black' }}>Venus</li>
        <li style={{ color: isDarkMode ? '#66bb6a' : 'black' }}>Earth</li>
        <li style={{ color: isDarkMode ? '#66bb6a' : 'black' }}>Mars</li>
        <li style={{ color: isDarkMode ? '#66bb6a' : 'black' }}>Jupiter</li>
        <li style={{ color: isDarkMode ? '#66bb6a' : 'black' }}>Saturn</li>
        <li style={{ color: isDarkMode ? '#66bb6a' : 'black' }}>Uranus</li>
        <li style={{ color: isDarkMode ? '#66bb6a' : 'black' }}>Neptune</li>
      </ul>
    </div>
  );
};

export default Slider;
